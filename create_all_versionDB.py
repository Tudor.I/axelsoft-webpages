import time
import PySimpleGUI as sg
import re
import os
import sys
from procedures.procedures_functions import *
from procedures.error_handling import *
from procedures.sql_commands import *
from pyodbc import OperationalError, InterfaceError

"""
   28/08/2021

    Program destinat crearii automate de pagini html folosind un anume set de date
de intrare. Se doreste sa simuleze programul create_all.prg scris in Visual FOX Pro
    Biblioteci externe folosite:
    1) PySimpleGUI -> folosit pentru a crea fereastra care primeste un anume input.
    2) mutegen -> pentru determinarea lungimii unui audio file

    NOTE: Varianta _versionDB va folosi baza de date ca si motor de cautare.
"""


def database_connection():
    """Functie responsabila de conexiunea cu baza de date"""
    try:
        with open("config.txt") as f:
            lines = f.readlines()
            if len(lines) == 2:
                con = conexiune_db(server=lines[0].strip(), database=lines[1].strip())
                if con == "server error":
                    sg.popup(
                        "Eroare de conexiune cu serverul.\nNumele este gresit.\n",
                        any_key_closes=True, auto_close=True, auto_close_duration=20)
                    res = generare_config(restore_config=True)
                    return "config" if res == "success" else res
                elif isinstance(con, tuple):
                    sg.popup("Baza de date nu exista. Necesita restaurarea",
                             any_key_closes=True, auto_close=True, auto_close_duration=20)
                    credentials = {"SERVER":lines[0].strip()}
                    res = MainApp.restore_backup(restore=True, credentials=credentials)
                    if res[0] == "error": return "error"
                    elif res[0] == "closed": return "closed"
                    else:
                        credentials['DATABASE'] = res[1]
                        res = generare_config(values=credentials)
                    return "config" if res[0] == "success" else "error"
                elif con == "Database name does not exist":
                    sg.popup("Baza de date din config.txt nu este corecta sau nu exista!",
                             any_key_closes=True, auto_close=True, auto_close_duration=20)
                    res = generare_config(restore_config=True)
                    return "config" if res == "success" else res
                else:
                    return con
            else:
                sg.popup(
                    "Datele din fisierul config.txt sunt incomplete!\n",
                    any_key_closes=True, auto_close=True, auto_close_duration=20)
                res = generare_config(restore_config=True)
                return "config" if res == "success" else res
    except FileNotFoundError:
        res = generare_config()
        if res == "success":
            sg.popup("Fisierul de config a fost creat cu succes!", any_key_closes=True, auto_close=True, auto_close_duration=20)
            return "generated"



class MainApp():
    sg.theme("DarkBrown2")
    def __init__(self):
        self.layout = [
            [sg.Titlebar("Create All")],
            [sg.Text("Introdu numele manualului: "), sg.InputText(key="-FILENAME-")],
            [sg.Button("Genereaza"), sg.Button("Anuleaza")],
            [sg.Text("_" * 700)],
            [sg.Text("Creaza un backup de siguranta: ", size=(25, 1)), sg.Button("Backup")],
            [sg.Text("Restaureaza baza de date: ", size=(25, 1)), sg.Button("Restaureaza")]
        ]
        self.window = sg.Window("Create all", self.layout, size=(600, 200), enable_close_attempted_event=True)

    def backup(self, connection):
        """Metoda ce este responsabila cu creare de backup"""
        layout = [
            [sg.Text("Pentru a crea backup, specificati folderul in care doriti sa se salveze backup-ul.")],
            [sg.Input(key="file_path"), sg.FolderBrowse()],
            [sg.Button("Creeaza"), sg.Cancel()]
        ]
        window = sg.Window("Backup", layout, enable_close_attempted_event=True)
        event, values = window.read()
        if (event == sg.WIN_CLOSE_ATTEMPTED_EVENT or event == "Cancel") and sg.popup_yes_no(
                "Sunteti sigur?") == "Yes":
            window.close()
            return
        elif event == "Creeaza":
            print(values['file_path'])
            cr = creare_backup(values["file_path"], connection)
            window.close()
            return cr

    @staticmethod
    def restore_backup(connection='', credentials=None, restore=False):
        """Metoda ce este responsabila cu restaurarea unei baze de date"""
        layout = [
            [sg.Titlebar("Restore Database")],
            [sg.Text("Pentru restaurarea bazei de date, selectati fisierul de backup (.bak) dorit.")],
            [sg.Input(key="file_backup_path"), sg.FileBrowse()],
            [sg.Button("Restaureaza"), sg.Cancel()]
        ]
        window = sg.Window("Backup", layout, enable_close_attempted_event=True)
        event, values = window.read()
        if event == "Cancel" or (event == sg.WIN_CLOSE_ATTEMPTED_EVENT and
                                 sg.popup_yes_no("Sunteti sigur") == "Yes"):
            window.close()
            return ("closed", None)
        elif event == "Restaureaza":
            if restore:
                rs = restore_database(values['file_backup_path'], connection, existdatabase=False,
                                      credentials=credentials)

            else:
                rs = restore_database(values['file_backup_path'], connection)
            if rs[0] == "success":
                sg.popup("Baza de date a fost restaurata cu succes!")
            else:
                sg.popup("Baza de date nu a putut fi restaurata!")
            window.close()
            return rs

    def window_load(self, manual):
        layout = [
            [sg.Text("Se genereaza noile paigini....")],
            [sg.ProgressBar(manual.pag, orientation='h', size=(20, 20), key="-PROGRESBAR-")],
            [sg.Cancel()]
        ]
        window = sg.Window("Create_all", layout)
        progres_bar = window['-PROGRESBAR-']

        # se deschide manual.htm si manual.html pentru a se efectua inlocuirile
        manual_htm = file_open("manual.htm")
        manual_html = file_open("manual.html")
        manual_htm000 = file_open("manual000.htm")
        manual_html000 = file_open("manual000.html")

        # Genereaza subsolul paginii .htm
        count = generare_coperta(manual_htm, manual_html, manual)
        if count == 0:
            counter_page_create = 2
            counter_page_already_exists = 0
        elif count == 1:
            counter_page_create = 1
            counter_page_already_exists = 1
        else:
            counter_page_create = 0
            counter_page_already_exists = 2
        if manual.pag_start == 0:
            start_index = 3
        else:
            start_index = manual.pag_start
        for page in range(start_index, manual.pag + 1):
            result = generare_pagini_carte(Manual=manual, pag=page, manual000_htm=manual_htm000,
                                           manual000_html=manual_html000)
            event, values = window.read(timeout=10)
            if result == 0:
                counter_page_create += 0
                counter_page_already_exists += 2
            elif result == 1:
                counter_page_create += 1
                counter_page_already_exists += 1
            else:
                counter_page_create += 2
                counter_page_already_exists += 0
            # aici va rula codul de generare a paginilor
            if event == "Cancel" or event == sg.WIN_CLOSED:
                cancel = False
                break
            progres_bar.update(page + 1)
        if counter_page_create > 1 and counter_page_already_exists == 0:
            sg.popup(f"Au fost generate {counter_page_create} pagini cu succes!", title="Outcome")
        if counter_page_create == 1 and counter_page_already_exists == 0:
            sg.popup(f"O pagina a fost generata cu succes!", title="Outcome")
        if counter_page_already_exists > 1 and counter_page_create == 0:
            sg.popup(f"{counter_page_already_exists} pagini deja exista! Paginile nu au mai fost create",
                     title="Outcome")
        if counter_page_already_exists == 1 and counter_page_create == 0:
            sg.popup(f"O pagina deja creata exista!", title="Outcome")
        if counter_page_already_exists >= 1 and counter_page_create >= 1:
            sg.popup(f"Au fost generate {counter_page_create} pagini cu succes!",
                     f"{counter_page_already_exists} pagini deja exista! Paginile nu au mai fost create",
                     title="Outcome")
        window.close()


def body_logic(con):
    while True:
        app = MainApp()
        event, values = app.window.read()
        if event == "Backup":
            n = app.backup(con)
            if n:
                sg.popup("Backup realizat cu succes!", title="Backup")
            else:
                sg.popup_error("Backup-ul nu s-a realizat!", title="Backup")
        if event == "Restaureaza":
            n = app.restore_backup(con)
            if n:
                sg.popup("Restaurarea bazei de date s-a realizat cu succes.\n"
                         "Se va inchide aplicatia.\n"
                         "Este necesar sa va reconectati.", title="Restore")
                app.window.close()
                break
            else:
                sg.popup("Restaurarea nu s-a realizat. A intervenit o eroare.", title="Restore")
        if (event == sg.WIN_CLOSE_ATTEMPTED_EVENT or event == "Anuleaza") and sg.popup_yes_no(
                "Doriti sa inchideti aplicatia?") == "Yes":
            app.window.close()
            con.close()
            break
        if event == "Genereaza":
            # sg.popup("Hello dude", "You are on the right track :D", f"Ai introdus urmatoarea valoare: {values['-FILENAME-']}")
            manual = generare_manual(con, values["-FILENAME-"])
            if manual is None:
                sg.popup("Ati introdus un nume de manual ce nu exista in baza de date!", title="Input invalid")
            else:
                vf = verificare_perequiste("engleza670")
                if vf == "manual.htm":
                    sg.popup("Nu exsite fisierul manual.htm")
                elif vf == "manual.html":
                    sg.popup("Nu exsite fisierul manual.html")
                elif vf == "manual000.htm":
                    sg.popup("Nu exsita fisierul manual000.htm")
                elif vf == "manual000.html":
                    sg.popup("Nu exista fisierul manual000.html")
                elif vf == manual.filename + "-image":
                    sg.popup("Nu exista directorul cu imagini")
                elif vf == manual.filename + "-audio":
                    sg.popup("Nu exista directorul cu melodii")
                else:
                    app.window_load(manual=manual)
        app.window.close()


def main():
    """Aici, in loc de variabila, va fi efectuata conexiunea cu baza de date,
    de unde vor fi aduse toate elementele necesare cu ajutorul carora se va crea o instanta de clasa Manual
    """
    # variabila de test pentru a testa generatorul de pagina
    con = database_connection()
    if con == "config":
        sg.popup("Fisierul de config a fost generat cu succes. Reporniti aplicatia",any_key_closes=True)
    elif con == "generated":
        sg.popup("Fisierul de config a fost generat cu succes. Reporniti aplicatia",any_key_closes=True)
    elif con == "error":
        sg.popup("A aparut o eroare. Incercati restaurarea fisierului config.\n"
                 "Daca acest mesaj persista, trimiteti problema la Tudor.", any_key_closes=True, auto_close_duration=20,
                 auto_close=True)
    elif con == "closed" or con == "close":
        pass
    else:
        body_logic(con)


if __name__ == "__main__":
    main()
    # DESKTOP-VTKFRFL\SQLEXPRESS
