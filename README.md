# axelsoft webpages

Project that aims to translate a dessktop app written in Visual FOX to Python with the aim of improving the overall apps and to migrate from Visual FOX to Python.

## Getting started

#### Perequisite

This app uses Microsoft SQL Server 2019 as database storage. You must have
installed SQL Server and MSSMS - Microsoft SQL Server Management Studio on your device. 
- [ ] [Microsoft SQL Server 2019](https://www.microsoft.com/en-us/Download/details.aspx?id=101064)
- [ ] [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)

The app is configured to run using Desktop authentication. For installation, you need
to provide your server name and a backup file.


### Create the project

Open cmd/bash console:
```
cd your_directory
git clone https://gitlab.com/Tudor.I/axelsoft-webpages.git
git pull

```

### Connect to the database

Once you have installed SQL Server Express and SQL Studio Management, launch the latter and
create your server. It is preferable to use Windows as OS, and choose Windows User Authentication. You can create 
a master User for your server, and then select SQL Authentication.


Note: It has not been tested on other OS, or with SQL Authentication. Further tests need to be run.


## Create all v0.1

The main goal to app is to generate pages that contains pages from a book (e.g. If a book has 130 pages, there will be generated 
130 pages) and to link each other together in a resulting book. There are 500 books that need to be generated.
Each book have TTS Audio and Images. The database contains all the books and the input neccesary to create 
this pages.

In the database, there are two tables (manuale, manuale_pag) that contains, combined, over 50.000 entries.

For this specific app, you must have two folders named 'book_name'-audio and 'book_name'-image that contains
 the appropiate files. (```book_name'-'nr_page'.mp3``` and ````'book_name'-'nr_page'.jpg````)

Mandatory files:

- [ ] manual.htm and manual.html
- [ ] manual000.htm and manual000.html

Mandatory directories:

- [ ] procedures and venv
- [ ] book_name-audio and book_name-image

#### create_all.exe

Once you have configured your database and checked the mandatories
 files, run create_all_versionDB.exe and have fun!
 
