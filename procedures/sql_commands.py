import datetime
import os
import random
import re

import pyodbc
from pyodbc import ProgrammingError, InterfaceError, OperationalError
from procedures.procedures_functions import *


def file_open(manualhtm):
    """Functie care deschide manual.htm sau manual.html si citeste continutul"""
    with open(manualhtm, encoding="utf-8") as f:
        data_before_con = f.readlines()
        data = ''.join(data_before_con)
    return data


def conexiune_db(server=None, database=None, restore=False, credentials=None):
    """Functie responsabila de conexiunea cu baza de date SQL Server"""
    if restore:
        connection_string = "Driver={ODBC Driver 17 for SQL Server};" \
                            f"Server={credentials['SERVER']};" \
                            "Trusted_Connection=yes;"
    else:
        connection_string = "Driver={ODBC Driver 17 for SQL Server};" \
                        f"Server={server};" \
                        f"Database={database};" \
                        f"Trusted_Connection=yes;"
    try:
        connection = pyodbc.connect(connection_string, autocommit=True)
    except OperationalError as e:
        string = "Server is not found or not accessible."
        match = re.search(string, str(e))
        if match is not None:
            return "server error"
    except InterfaceError as e:
        string = 'Cannot open database "%s" requested by the login' % (database)
        match = re.search(string, str(e))
        if match is not None and re.search("manual", str(e)) is not None:
            return ("Restore database", database)
        elif re.search("manual", str(e)) is None:
            return "Database name does not exist"

    """string = "ALTER DATABASE manual SET OFFLINE WITH ROLLBACK IMMEDIATE;"
    cn.execute(string)
    """
    return connection.cursor()


def creare_backup(folder_path, conn):
    """Functie responsabila pentru crearea backup-ului"""
    if folder_path:
        USE_STRING = "USE manual;"
        time_creation = datetime.datetime.now()
        file_path = (folder_path+ r"\'" +str(time_creation.year) + "_" + str(time_creation.month) + "_" +
                     str(time_creation.second) + "_backup_" + str(random.randint(1, 3000000000)) + ".bak")
        file_path = file_path.replace("'", "")
        file_path = file_path.replace("C:/", r"C:\'")
        file_path = file_path.replace("'", "")
        print(file_path)
        CREATE_BACKUP_STRING = f"BACKUP DATABASE manual TO DISK = '{file_path}';"
        conn.execute(USE_STRING)
        conn.execute(CREATE_BACKUP_STRING)
        return file_path
    else:
        return


def restore_database(backup_path, conn, existdatabase=True, credentials=None):
    """Functie responsabila pentru restaurarea bazei de date"""
    bak = ".bak"
    if existdatabase is False:
        conn = conexiune_db(credentials=credentials,restore=True)
    if os.path.isfile(backup_path) and bak in backup_path:
        OFFLINE_COMMAND = "ALTER DATABASE manual SET OFFLINE WITH ROLLBACK IMMEDIATE;"
        DROP_DATABASE = "DROP DATABASE IF EXISTS manual;"
        SWITCH_MASTER = "USE master;"
        RESTORE_COMMAND = F"RESTORE DATABASE manual FROM DISK = " \
                          F"'{backup_path}' WITH REPLACE;"
        if existdatabase:
            conn.execute(OFFLINE_COMMAND)
        conn.execute(DROP_DATABASE)
        conn.execute(SWITCH_MASTER)
        try:
            conn.execute(RESTORE_COMMAND)
        except ProgrammingError as e:
            RESTORE_COMMAND = "RESTORE DATABASE [manual] FROM DISK = '%s' WITH REPLACE, RECOVERY, " % (backup_path)
            FILESONLY = f"RESTORE FILELISTONLY FROM DISK = '{backup_path}';"
            res = conn.execute(FILESONLY).fetchall()
            error = str(e)
            error_find_string1 = "[SQL Server]Directory lookup for the file"
            error_find_string2 = "failed with the operating system error 3(The system cannot find the path specified.)"
            if error.find(error_find_string1) != -1 and error.find(error_find_string2) != -1:
                mdf_file = res[0][0]
                path_mdf_file = res[0][1].replace("Microsoft SQL Server", "Microsoft SQL Server Express")
                log_file = res[1][0]
                path_log_file = res[1][1].replace("Microsoft SQL Server", "Microsoft SQL Server Express")
                RESTORE_COMMAND += "MOVE '%s' TO '%s', " % (mdf_file, path_mdf_file)
                RESTORE_COMMAND += "MOVE '%s' TO '%s';" % (log_file, path_log_file)
                conn.execute(RESTORE_COMMAND)
                print(RESTORE_COMMAND, "SUNT PE ERROR")
        while conn.nextset():
            pass
        conn.close()
        return ("success", "manual")
    else:
        return ("error", None)

if __name__ == "__main__":
    a = "sada.bak"
    b = ".bak"
    print( b in a)
