class Error(Exception):
    """Clasa de baza pentru erorile custom - mostenste Exception"""
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)

class FileExist(Error):
    """Eroare care va fi aruncata in momentul in care exista deja un fisier creat"""
    def __init__(self):
        Error.__init__(self, message="Fisierul deja exista")
