import re
import os
import datetime
import random

import regex
import pyodbc
from pyodbc import InterfaceError, OperationalError
from mutagen.mp3 import MP3
from procedures.error_handling import *
import PySimpleGUI as sg


class Manual:
    def __init__(self, manual, link_pdf, link_md, title, clasa, lang, isbn5_pag, nrPag, pag_start):
        self.filename = manual
        self.link_pdf = link_pdf.strip()
        self.link_md = link_md.strip()
        self.isbn_5 = isbn5_pag.strip()
        self.clasa = clasa.strip()
        self.limba = lang.strip()
        self.pag_start = pag_start
        self.pag = nrPag
        self.title = title.strip()

    def __repr__(self):
        string = f"Manual:{self.filename}\n" \
                 f"Link_PDF:{self.link_pdf}\n" \
                 f"Link_MD:{self.link_md}\n" \
                 f"ISBN_5:{self.isbn_5}\n" \
                 f"CLASA:{self.clasa}\n" \
                 f"LIMBA:{self.limba}\n" \
                 f"PAG_START:{self.pag_start}\n" \
                 f"PAG:{self.pag}\n" \
                 f"TITLE:{self.title}\n"
        return string


def generare_txt(values):
    with open("config.txt", "w+", encoding="utf-8") as f:
        f.writelines([values['SERVER'] + "\n" + values['DATABASE']])
    curr_dir = os.getcwd()
    file_path = curr_dir + r"'\'" + "config.txt"
    file_path = file_path.replace("'", "")
    if os.path.isfile(file_path):
        return True
    else:
        return False


def generare_config(restore_config=False, values=None):
    print(values)
    if values is not None:
        file = generare_txt(values)
        if file:
            return ("success", None)
        else:
            return ("error", None)
    else:
        if restore_config:
            sg.popup("Fisierul config va fi generat din nou",
                     any_key_closes=True, auto_close=True, auto_close_duration=20)
        else:
            sg.popup("Fisierul config nu exista. Acesta va fi generat din nou",
                     any_key_closes=True, auto_close=True, auto_close_duration=20)
        layout = [
            [sg.Titlebar("Config")],
            [sg.Text("Nume server: ", size=(10, 1)), sg.InputText(key='SERVER', size=(40, 1))],
            [sg.Text("Nume baza de date: ", size=(10, 1)), sg.InputText(key='DATABASE', size=(40, 1))],
            [sg.Text("_" * 390)],
            [sg.Button("Generare", key="Generare config"), sg.Button("Anuleaza", key="CANCEL")],
        ]
        window = sg.Window("Config", layout, size=(390, 180))
        while True:
            event, valuess = window.read()
            if event == "Generare config":
                if (valuess['SERVER'] != "" and valuess['DATABASE'] == "") or (valuess['SERVER'] == "" and valuess['DATABASE'] != ""):
                    sg.popup("Nu puteti lasa campuri goale!",title="Error", any_key_closes=True)
                else:
                    file = generare_txt(valuess)
                    if file:
                        window.close()
                        return "success"
                    else:
                        sg.popup("Nu s-a putut genera fisierul de config.\nM-ai incercati o data",
                                 any_key_closes=True, auto_close=True, auto_close_duration=20)
            if event == "CANCEL" or event == sg.WINDOW_CLOSED:
                window.close()
                return "close"


def verificare_perequiste(Manual):
    """Functie ce este apelata in momentul in care se doreste generarea paginilor. Verifica ca directorul sa contina
    fisierele si directoarele obligatorii pentru a se putea genera paginile. Se verifica existenta:
        1)manual.htm, manual.html, manual000_html, manual000_htm
        2)directoarele denumite 'numele cartii'-audio, 'numele cartii'-image si 'numele cartii'-qr
            2.1)Se verifica si existenta continutului.
        3)directorul procedures
        4)directorul venv
        Daca aceste conditii nu sunt intrunite, programul nu va rula si va arunca erorile aferente
        """
    fileName = Manual
    manual_htm = "manual.htm"
    manual_html = "manual.html"
    manual000_htm = "manual000.htm"
    manual000_html = "manual000.html"
    IMAGE_DIR = fileName + "-image"
    AUDIO_DIR = fileName + "-audio"
    working_directory = os.getcwd()
    wd_files = wd_dir = []
    for root, dir, files in os.walk(working_directory):
        wd_files = [file for file in files]
        wd_dir = [d for d in dir]
        break
    if manual_htm not in wd_files: return manual_htm
    if manual_html not in wd_files: return manual_html
    if manual000_html not in wd_files: return manual000_html
    if manual000_htm not in wd_files: return manual000_htm
    if IMAGE_DIR not in wd_dir: return IMAGE_DIR
    if AUDIO_DIR not in wd_dir: return AUDIO_DIR


def generare_manual(cnxn, fileName):
    """Functie care merge si cauta in baza de date numele manualului dat ca si input
        :return: None - daca nu exista manualul. Instanta de clasa Manual, daca exista in DB"""

    comamand_use = "use manual;"
    string_columns = "manual, link_pdf, link_md, isbn_5, clasa, limba, pag_start, pag, title"
    command_select = f"SELECT {string_columns} FROM dbo.manuale WHERE manual = '{fileName}';"
    cnxn.execute(comamand_use)
    data = cnxn.execute(command_select).fetchone()
    if data is not None:
        inst_manual = Manual(
            manual=data[0],
            link_pdf=data[1],
            link_md=data[2],
            isbn5_pag=data[3],
            clasa=data[4],
            lang=data[5],
            pag_start=int(data[6]) if data[6].isdigit() else 0,
            nrPag=int(data[7]) if data[7].isdigit() else 0,
            title=data[8]
        )
        print(data[6])
        return inst_manual
    else:
        return None


def pagina(target_string, nr_pages, pag_start, fileName):
    """Adauga nr_paginii la subsol"""
    pattern = "<!--Pagina: -->"
    if pag_start == 0:
        start_index = 3
    else:
        start_index = pag_start
    result_string = "<p class='style1'>" \
                    "<span class='style1'>Deschide rapid la pagina:"
    for i in range(start_index, nr_pages + 1):
        if i < 10:
            local_string = f"<a target='text' href='{fileName}-00{i}.htm'>{i} </a>\n"
            result_string += local_string
        elif i >= 10 and i < 100:
            local_string = f"<a target='text' href='{fileName}-0{i}.htm'>{i} </a>\n"
            result_string += local_string
        else:
            local_string = f"<a target='text' href='{fileName}-{i}.htm'>{i} </a>\n"
            result_string += local_string
    result_string += "</span></p>"
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, pattern + "\n" + result_string)
    return [target_string, result_string]


def greseli(target_string, filename):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
    returneaza noul string"""
    pattern = "<!--Greşeli de redactare şi sugestii-->"
    local_string = "<!--Greşeli de redactare şi sugestii-->\n" \
                   "<p class='style2'>\n" \
                   f"<a target='_blank' href='{filename}" + "-g.htm'>\n" \
                                                            f"<span class='style3'>\n" \
                                                            f"<img alt='' src='bug.jpg' />\n" \
                                                            f"Greșeli de redactare și sugestii</span></a></p>\n"
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, local_string)
    return target_string


def pronuntarea(target_string, filename):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
     returneaza noul string"""
    pattern = "<!--Pronunţarea corectă a numelor proprii străine-->"
    local_string = "<!--Pronunţarea corectă a numelor proprii străine-->\n" \
                   "<p class='style4'>\n" \
                   f"<a target='_blank' href='{filename}" + "-p.htm'>\n" \
                                                            f"<span class='style4'>\n" \
                                                            f"<img alt='' src='pronuntiation-40.jpg'/>\n" \
                                                            "Pronunțarea corectă a numelor proprii străine</span></a></p>\n"
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, local_string)
    return target_string


def tts(target_string, filename):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
    returneaza noul string"""
    pattern = "<!--Text to Speech (-tts.htm) -->"
    local_string = '<p class="style2">\n' \
                   f'<a target="_blank" href="{filename}' + '-tts.htm">\n' \
                                                            '<span class="style2">\n' \
                                                            '<img alt="" src="listen-40.jpg" />\n' \
                                                            'Textul manualului (pentru a genera audio, conform OMEN \n' \
                                                            'nr. 3124/2017)</span></a></p> \n'
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, local_string)
    return target_string


def pdf(target_string, manual):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
    returneaza noul string"""
    if manual.link_pdf != '':
        pattern = "<!--Manualul în format .pdf-->"
        local_string = '<!--Manualul în format .pdf-->\n' \
                       '<p class="style2">\n' \
                       f'<a target="_blank" href="{manual.link_pdf}">\n' \
                       '<span class="style2">\n' \
                       '<img alt="" src="pdf-40.jpg" />\n' \
                       'Manualul în format .pdf (Încărcarea durează câteva minute)\n' \
                       '</span></a></p>\n'
        if re.search(pattern, target_string) is not None:
            target_string = target_string.replace(pattern, local_string)
    return target_string


def vocabulary(target_string, filename):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
        returneaza noul string. De asemenea, in functie de numele file-ului introdus, inlocuieste substring-ul 'Vocabulary'"""
    pattern = "<!--Vocabulary-->"
    local_string = '<!--Vocabulary-->\n' \
                   '<p class="style2">\n' \
                   f'<a target="_blank" href="{filename}' + '-v.html">\n' \
                                                            '<span class="style2">\n' \
                                                            '<img alt="" src="vocabulary-50.jpg" />\n' \
                                                            'Vocabulary</span></a></p>\n'
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, local_string)
    if 'engleza' in filename:
        target_string = target_string.replace('Vocabulary', 'English-Romanian Vocabulary\n')
    elif filename == 'franceza':
        target_string = target_string.replace('Vocabulary', 'Vocabulaire franco-romain\n')
    elif filename == "germana":
        target_string = target_string.replace("Vocabulary", "Deutsch-rÃ¶misches Vokabular\n")
    return target_string


def rasfoieste(target_string, manual):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
        returneaza noul string."""
    if manual.link_md != '':
        pattern = "<!--Răsfoiește manualul-->"
        local_string = '<!--Răsfoiește manualul-->\n' \
                       '<p class="style2">\n' \
                       f'<a target="_blank" href="{manual.link_md}">\n' \
                       f'<span class="style2">\n' \
                       f'<img alt="" src="flip-40.jpg" />\n' \
                       f'Răsfoiește manualul</span></a></p>\n'
        if re.search(pattern, target_string) is not None:
            target_string = target_string.replace(pattern, local_string)
    return target_string


def md(target_string, manual):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
            returneaza noul string."""
    if manual.link_md != '':
        pattern = "<!--Manualul în format digital-->"
        local_string = '<!--Manualul în format digital-->\n' \
                       '<p class="style2">\n' \
                       f'<a target="_blank" href="{manual.link_md}">\n' \
                       f'<span class="style2">\n' \
                       f'<img alt="" src="cd.jpg" />\n' \
                       f'Manualul în format digital</span></a></p>\n'
        if re.search(pattern, target_string) is not None:
            target_string = target_string.replace(pattern, local_string)
    return target_string


def coperta(target_string, filename, manual):
    """Inlocuieste în string-ul target(pagina html suport) variabila pattern, acolo unde este cazul, cu local_string si
            returneaza noul string."""
    pattern = '<!--Coperta-->'
    if manual.link_md != '':
        local_string = '<!--Coperta-->' \
                       '<p class="style2">\n' \
                       'Clic pe manual pentru a-l răsfoi sau<br />\n' \
                       'clic pe <em>Pagina următoare</em> sau <br />\n' \
                       'clic pe numărul paginii de mai jos<br />\n' \
                       f'<a target="_blank" href="{manual.link_md}">\n' \
                       f'<img alt="" class="center-fit" src="{filename}-t.jpg" /></a></p>\n'
    else:
        local_string = '<!--Coperta-->\n' \
                       '<p class="style2">\n' \
                       f'<img alt="" class="center-fit" src="{filename}-t.jpg" /></p>\n'
    if re.search(pattern, target_string) is not None:
        target_string = target_string.replace(pattern, local_string)
    return target_string


# 1) Se creaza un nou director denumit - Pagini generate
# 2) Se vor face modificari in string-ul, "MANUAL.HTM" respectiv "MANUAL.HTML", unde se vor apela in parte pentru fiecare pagina
# in parte, functiile(procedurile) descrise mai sus
# 3) O data ce a fost generat noul string, se vor scrie pagina cu pagina
# 4) Paginile vor fi salvate in format .html in directorul Pagini generate


# def creare_director():
#     """
#     Functie responsabila de crearea directorului unde vor fi salvate toate paginile.
#     Returneaza calea directorului, in care vor fi salvate paginile. Daca directorul exista deja, logica merge
#     mai departe.
#     """
#     dir_name = "pagini-generate"
#     working_directory = os.getcwd()
#     path = os.path.join(working_directory, dir_name)
#     try:
#         os.mkdir(path)
#     except FileExistsError:
#         pass
#     return path

def convert(seconds):
    minutes = seconds // 60
    seconds %= 60
    return minutes, seconds


def audio_length(audio_path):
    file = MP3(audio_path)
    durata_secunde = file.info.length
    minute, secunde = convert(durata_secunde)
    minute = int(minute)
    secunde = int(secunde)
    if secunde < 10:
        secunde = "0" + str(secunde)
    return "%s:%s" % ("0" + str(minute), secunde)


def generare_pagini_carte(manual000_htm, manual000_html, Manual, pag):
    """
    Functie responsabila de generarea paginilor prin corelarea audio cu imaginea aferenta si inlocuirea
    lor template
    :param manual000_htm: string-ul manual000_htm (template) in care se vor face inlocuirile corespunzatoare
    :param manual000_html: string-ul manual000_html(template) in care se vor face inlocuirile corespunzatoare (joaca rol
    de iframe)
    :param Manual: Instanta de clasa Manual care va contine datele necesare pt crearea de pagini
    :return:
    """
    fileName = Manual.filename
    AUDIO_DIR = fileName + '-audio'
    IMAGE_DIR = fileName + '-image'
    QR_DIR = fileName + '-qr'  # va fi folosit in cazul in care se doreste utilizarea QR code ului pe site

    start_index = Manual.pag_start if Manual.pag_start > 0 else 3
    nr_pages = Manual.pag

    # 1) Subsolul cu pagini generat
    subsol_pagini = pagina(manual000_htm, Manual.pag, Manual.pag_start, fileName)[1]
    # 2) Variabilele care stocheaza template-urile pt .htm si .html
    manual_html_iframe = manual000_html
    manual_htm = manual000_htm

    # 3) Variabile care contin string ce trebuie inlocuit in template
    manual_htm0 = "manual000.htm"
    audio_href_target = "manual000-audio/manual000.mp3"
    audio_duration_target = "Durata audio 0:01"
    pagina_pregedenta = "manual000-1.htm"
    href_c_htm = "manual000-c.htm"
    image_href_target = "manual000-image/manual000.jpg"
    pagina_urmatoare = "manual000+1.htm"
    pagini = "<!--Pagina: -->"

    # locatia in director unde vor fi generate paginile
    path_for_file_htm = os.getcwd() + r"\'" + fileName
    path_for_file_html = os.getcwd() + r"\'" + fileName
    path_for_file_html = path_for_file_html.replace("'", "")
    path_for_file_htm = path_for_file_htm.replace("'", "")
    counter = 0
    if pag < 10:
        # 1) generam path-ul catre cartea corespunzatoare pt  .htm si .html(path-ul catre)
        path_for_file_htm = path_for_file_htm + f"-00{pag}.htm"
        path_for_file_html = path_for_file_html + f"-00{pag}.html"
        if not os.path.isfile(path_for_file_htm):
            # 2) Facem modificarile in string
            # 2.1) -> Stabilim conexiunea cu audio
            audio_href_target_for_replacing = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-00{pag}.mp3"
            audio_href_target_for_replacing = audio_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_htm.replace(audio_href_target, audio_href_target_for_replacing)
            # 2.2) - Adaugam durata audio-ului in template
            audio_len = audio_length(audio_href_target_for_replacing)
            duration_string = f"Durata audio {audio_len}"
            manual_to_be_generated = manual_to_be_generated.replace(audio_duration_target,
                                                                    duration_string)
            # 2.3) - Adaugam pagina precedenta
            precedent_page_for_change = f"{fileName}.htm" if pag == start_index else f"{fileName}-00{pag - 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_pregedenta, precedent_page_for_change)

            # 2.3) - Adaugam filename-c.htm
            manual_to_be_generated = manual_to_be_generated.replace(href_c_htm, f"{fileName}-c.html", 2)

            # 2.3) - Adaugam jpeg.ul din folder
            image_href_target_for_replacing = f"{IMAGE_DIR}" + r"\'" + f"{fileName}-00{pag}.jpg"
            image_href_target_for_replacing = image_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_to_be_generated.replace(image_href_target, image_href_target_for_replacing)
            # 2.4) - Adaugam pagina urmatoare
            next_page = f"{fileName}-00{pag + 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_urmatoare, next_page)
            # 2.5) - Adaugam paginile in subsolul paginii
            manual_to_be_generated = manual_to_be_generated.replace(pagini, subsol_pagini)
            # 2.6) Cream .htm
            with open(path_for_file_htm, "w+", encoding="UTF-8") as f:
                f.write(manual_to_be_generated)
            counter += 1

        # 3) Generarea paginii .html care contine iframe
        if not os.path.isfile(path_for_file_html):
            src_audio_iframe = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-00{pag}.mp3"
            src_audio_iframe = src_audio_iframe.replace("'", "")
            manual_html_iframe = manual_html_iframe.replace(audio_href_target, src_audio_iframe)
            htmo0_page = f"{fileName}-00{pag}.htm"
            manual_html_iframe = manual_html_iframe.replace(manual_htm0, htmo0_page)
            # 4.2) Cream html
            with open(path_for_file_html, "w+", encoding="UTF-8") as f:
                f.write(manual_html_iframe)
            counter += 1
    elif pag >= 10 and pag < 100:
        # 1) generam path-ul catre cartea corespunzatoare pt  .htm si .html(path-ul catre)
        path_for_file_htm = path_for_file_htm + f"-0{pag}.htm"
        path_for_file_html = path_for_file_html + f"-0{pag}.html"
        if not os.path.isfile(path_for_file_htm):
            # 2) Facem modificarile in string
            # 2.1) -> Stabilim conexiunea cu audio
            audio_href_target_for_replacing = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-0{pag}.mp3"
            audio_href_target_for_replacing = audio_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_htm.replace(audio_href_target, audio_href_target_for_replacing)
            # 2.2) - Adaugam durata audio-ului in template
            audio_len = audio_length(audio_href_target_for_replacing)
            duration_string = f"Durata audio {audio_len}"
            manual_to_be_generated = manual_to_be_generated.replace(audio_duration_target,
                                                                    duration_string)
            # 2.3) - Adaugam pagina precedenta
            precedent_page_for_change = f"{fileName}-00{pag - 1}.htm" if pag - 1 < 10 else f"{fileName}-0{pag - 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_pregedenta, precedent_page_for_change)

            # 2.3) - Adaugam filename-c.htm
            manual_to_be_generated = manual_to_be_generated.replace(href_c_htm, f"{fileName}-c.html", 2)

            # 2.3) - Adaugam jpeg.ul din folder
            image_href_target_for_replacing = f"{IMAGE_DIR}" + r"\'" + f"{fileName}-0{pag}.jpg"
            image_href_target_for_replacing = image_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_to_be_generated.replace(image_href_target, image_href_target_for_replacing)
            # 2.4) - Adaugam pagina urmatoare
            next_page = f"{fileName}-0{pag + 1}.htm" if pag + 1 < 100 else f"{fileName}-{pag + 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_urmatoare, next_page)
            # 2.5) - Adaugam paginile in subsolul paginii
            manual_to_be_generated = manual_to_be_generated.replace(pagini, subsol_pagini)

            with open(path_for_file_htm, "w+", encoding="UTF-8") as f:
                f.write(manual_to_be_generated)
            counter += 1
        # 3) Generarea paginii .html care contine iframe

        if not os.path.isfile(path_for_file_html):
            src_audio_iframe = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-0{pag}.mp3"
            src_audio_iframe = src_audio_iframe.replace("'", "")
            manual_html_iframe = manual_html_iframe.replace(audio_href_target, src_audio_iframe)
            htmo0_page = f"{fileName}-0{pag}.htm"
            manual_html_iframe = manual_html_iframe.replace(manual_htm0, htmo0_page)
            with open(path_for_file_html, "w+", encoding="UTF-8") as f:
                f.write(manual_html_iframe)
            counter += 1

    else:
        path_for_file_htm = path_for_file_htm + f"-{pag}.htm"
        path_for_file_html = path_for_file_html + f"-{pag}.html"
        if not os.path.isfile(path_for_file_htm):
            # 1) generam path-ul catre cartea corespunzatoare pt  .htm si .html(path-ul catre)
            # 2) Facem modificarile in string
            # 2.1) -> Stabilim conexiunea cu audio
            audio_href_target_for_replacing = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-{pag}.mp3"
            audio_href_target_for_replacing = audio_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_htm.replace(audio_href_target, audio_href_target_for_replacing)
            # 2.2) - Adaugam durata audio-ului in template
            audio_len = audio_length(audio_href_target_for_replacing)
            duration_string = f"Durata audio {audio_len}"
            manual_to_be_generated = manual_to_be_generated.replace(audio_duration_target,
                                                                    duration_string)
            # 2.3) - Adaugam pagina precedenta
            precedent_page_for_change = f"{fileName}-0{pag - 1}.htm" if pag - 1 < 100 else f"{fileName}-{pag - 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_pregedenta, precedent_page_for_change)

            # 2.3) - Adaugam filename-c.htm
            manual_to_be_generated = manual_to_be_generated.replace(href_c_htm, f"{fileName}-c.html", 2)

            # 2.3) - Adaugam jpeg.ul din folder
            image_href_target_for_replacing = f"{IMAGE_DIR}" + r"\'" + f"{fileName}-{pag}.jpg"
            image_href_target_for_replacing = image_href_target_for_replacing.replace("'", "")
            manual_to_be_generated = manual_to_be_generated.replace(image_href_target, image_href_target_for_replacing)
            # 2.4) - Adaugam pagina urmatoare
            next_page = f"{fileName}-0{pag + 1}.htm" if pag + 1 < 100 else f"{fileName}-{pag + 1}.htm"
            manual_to_be_generated = manual_to_be_generated.replace(pagina_urmatoare, next_page)
            # 2.5) - Adaugam paginile in subsolul paginii
            manual_to_be_generated = manual_to_be_generated.replace(pagini, subsol_pagini)
            with open(path_for_file_htm, "w+", encoding="UTF-8") as f:
                f.write(manual_to_be_generated)
            counter += 1
            # 3) Generarea paginii .html care contine iframe
        if not os.path.isfile(path_for_file_html):
            src_audio_iframe = f"{AUDIO_DIR}" + r"\'" + f"{fileName}-{pag}.mp3"
            src_audio_iframe = src_audio_iframe.replace("'", "")
            manual_html_iframe = manual_html_iframe.replace(audio_href_target, src_audio_iframe)
            htmo0_page = f"{fileName}-{pag}.htm"
            manual_html_iframe = manual_html_iframe.replace(manual_htm0, htmo0_page)
            with open(path_for_file_html, "w+", encoding="UTF-8") as f:
                f.write(manual_html_iframe)
            counter += 1
    path_for_file_htm = os.getcwd() + r"\'" + fileName
    path_for_file_html = os.getcwd() + r"\'" + fileName
    path_for_file_html = path_for_file_html.replace("'", "")
    path_for_file_htm = path_for_file_htm.replace("'", "")
    return counter


def generare_coperta(manual_htm, manual_html, Manual):
    """"
    Returneaza crearea a doua pagini web. filename.html si filename.htm si :param count care stocheaza nr de pagini create
    la o apelare.
    :param fileName: numele manualului dat ca si input de user
    :param Manual: instanta de clasa Manual cu datele aferente.
    :param manual_htm: string-ul manual.htm in care se vor face inlocuirile pt pagina de inceput(coperta)
    :param manual_html: string-ul manual.html in care se vor face inlocuirile pt pagina de inceput(coperta) care joaca
    rol de iframe

    """
    fileName = Manual.filename
    AUDIO_DIR = fileName + '-audio'
    IMAGE_DIR = fileName + '-image'
    QR_DIR = fileName + '-qr'

    # 1) Se creaza un nou director denumit pagini generate. Se stocheaza intr-o variabila, calea unde vor fi stocate
    # fisierele generate
    path_for_files = os.getcwd()
    # stocam intr-o lista fisierele din directorul pagini-create (testam la inceput sa vedem daca exista)
    # path_for_parrent_directory = os.path.normpath(os.getcwd() + os.sep + os.pardir) -> cod pentru determina directorul parinte
    # al directorului curent

    # generam path-ul catre mp3 ul destinam paginii de inceput
    list_of_music_files = []
    path_for_parrent_directory = os.getcwd()
    AUDIO_PATH = path_for_parrent_directory + r'\"' + AUDIO_DIR
    AUDIO_PATH = AUDIO_PATH.replace('"', '')
    for root, dirs, files in os.walk(AUDIO_PATH):
        for file in files.__reversed__():
            if file == f"{fileName}.mp3":
                list_of_music_files.append(file)
                break
        break

    # 2) Se vor face modificari in string-ul, "MANUAL.HTM" respectiv "MANUAL.HTML", unde se vor apela in parte pentru fiecare pagina
    # in parte, functiile(procedurile) descrise mai sus

    # 2.1 ) -> Se vor face modificari prima daata in manual.htm. Se apeleaza FUNCTIILE (PROCEDURILE) ce vor face
    # inlocuiri in string. Se apeleaza inlocuirile standard

    local_manual = pagina(manual_htm, Manual.pag, Manual.pag_start, fileName)[0]
    local_manual = greseli(local_manual, fileName)
    local_manual = pronuntarea(local_manual, fileName)
    local_manual = tts(local_manual, fileName)
    local_manual = pdf(local_manual, Manual)
    local_manual = vocabulary(local_manual, fileName)
    local_manual = rasfoieste(local_manual, Manual)
    local_manual = md(local_manual, Manual)
    local_manual = coperta(local_manual, fileName, Manual)

    # 2.2) - Se fac inlocuri extra. Se inlocuieste in manual.htm stringul manual000-003.htm si manual000-004.htm
    to_be_replaced = 'manual000-003.htm'
    next_page = 'manual000-004.htm'
    audio_href = "manual000-audio/manual000.mp3"
    replaced = f'{fileName}-003.htm' if Manual.pag_start == 0 else f'{fileName}-00{Manual.pag_start}'
    next_page_replaced = f'{fileName}-004.htm' if Manual.pag_start == 0 else f'{fileName}-00{Manual.pag_start + 1}'
    audio_href_to_be_replaced = r'%s-audio/%s' % (fileName, list_of_music_files[0])
    local_manual = local_manual.replace(to_be_replaced, replaced)
    local_manual = local_manual.replace(next_page, next_page_replaced)
    local_manual = local_manual.replace(audio_href, audio_href_to_be_replaced)

    # se creaza path-ul pentru a genera noile fisiere
    htm_path_file = path_for_files + r"\'" + f'{fileName}.htm'
    htm_path_file = htm_path_file.replace("'", "")
    # se creaza noul fisier

    # generam fisierul .html care va contine iframe-ul
    src_iframe_manualhtm = "manual000.htm"
    src_iframe_audio = audio_href

    html_path_file = path_for_files + r"\'" + f'{fileName}.html'
    html_path_file = html_path_file.replace("'", "")

    local_iframe = manual_html.replace(src_iframe_manualhtm, f'{fileName}.htm')
    local_iframe = local_iframe.replace(src_iframe_audio, audio_href_to_be_replaced)

    count = 0
    if os.path.isfile(htm_path_file):
        count += 1
    else:
        with open(htm_path_file, "w+", encoding='utf-8') as f:
            f.write(local_manual)
    if os.path.isfile(html_path_file):
        count += 1
    else:
        with open(html_path_file, "w+", encoding='utf-8') as f:
            f.write(local_iframe)
    return count


if __name__ == "__main__":
    creare_backup("a", "a")

""""
PAsii pentru realizarea urmatoarei faze a aplicatiei:
1) Se creaza casuta login:
    1.1) Va contine - Server name, Username si Parola.
    1.2) Se va realiza conexiunea, iar daca totul este in regula, va aparea window-ul principal, altfel, popup_error
2) In acest caz, casuta de generare, va mai contine inca 2 butoane:
    2.1) Import database -> la import, ca si parametru se va primi calea fisierului. se va deschide un window pentru a se
    cauta fisierul .bak si se va lua calea, apoi vor fi rulate comenzi SQL In spate pt a se face importul de date 
    in siguranta
    2.2) Realizeaza backup -> la apasarea butonului, se vor face doua cazuri: pe discul C:\ , daca nu exista
    directorul backup, se va crea directorul, si se va efectua backup-ul,
    iar daca exista, se va crea fisierul .bak acolo automat, iar la final va aparea pe window, BACKUP-ul s-a realizat
    cu succes. .bak se gaseste in folder-ul (si se pune path-ul respectiv

"""
