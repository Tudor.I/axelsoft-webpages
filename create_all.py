import time
import PySimpleGUI as sg
import re
import os
import sys
import pymysql

from procedures.procedures_functions import *
from procedures.error_handling import *

"""
        26/08/2021
            
    Program destinat crearii automate de pagini html folosind un anume set de date
de intrare. Se doreste sa simuleze programul create_all.prg scris in Visual FOX Pro
    Biblioteci externe folosite:
    1) PySimpleGUI -> folosit pentru a crea fereastra care primeste un anume input.
    2) mutegen -> pentru determinarea lungimii unui audio file
    
    NOTE: Varianta de fata se foloseste fara conexiune catre baza de date.
    
    
"""


class MainApp():
    sg.theme("DarkBrown2")
    def __init__(self):
        self.layout = [
            [sg.Titlebar("Create All")],
            [sg.Text("Introdu numele manualului: "), sg.InputText(key="-FILENAME-")],
            [sg.Button("Genereaza"), sg.Button("Anuleaza")]
        ]
        self.window = sg.Window("Create all", self.layout, size=(700,100), enable_close_attempted_event=True)

    def window_load(self, fileName, manual):
        layout = [
            [sg.Text("Se genereaza noile paigini....")],
            [sg.ProgressBar(manual.pag, orientation='h', size=(20,20), key="-PROGRESBAR-")],
            [sg.Cancel()]
        ]
        window = sg.Window("Create_all", layout)
        progres_bar = window['-PROGRESBAR-']
        cancel = True

        # se deschide manual.htm si manual.html pentru a se efectua inlocuirile
        manual_htm = file_open("manual.htm")
        manual_html = file_open("manual.html")
        manual_htm000 = file_open("manual000.htm")
        manual_html000 = file_open("manual000.html")

        #Genereaza subsolul paginii .htm
        count = generare_coperta(fileName,manual_htm, manual_html,manual)
        if count == 0:
            counter_page_create = 2
            counter_page_already_exists = 0
        elif count == 1:
            counter_page_create = 1
            counter_page_already_exists = 1
        else:
            counter_page_create = 0
            counter_page_already_exists = 2
        if manual.pag_start== 0:
            start_index = 3
        else:
            start_index = manual.pag_start
        for page in range(start_index, manual.pag+1):
            result = generare_pagini_carte(fileName=fileName, Manual=manual, pag=page, manual000_htm=manual_htm000, manual000_html=manual_html000)
            event, values = window.read(timeout=10)
            if result == 0:
                counter_page_create += 0
                counter_page_already_exists += 2
            elif result == 1:
                counter_page_create+=1
                counter_page_already_exists+=1
            else:
                counter_page_create+=2
                counter_page_already_exists+=0
            #aici va rula codul de generare a paginilor
            if event == "Cancel" or event == sg.WIN_CLOSED:
                cancel = False
                break
            progres_bar.update(page+1)
        if counter_page_create > 1 and counter_page_already_exists == 0:
            sg.popup(f"Au fost generate {counter_page_create} pagini cu succes!", title="Outcome")
        if counter_page_create == 1 and counter_page_already_exists == 0:
            sg.popup(f"O pagina a fost generata cu succes!", title="Outcome")
        if counter_page_already_exists > 1 and counter_page_create == 0:
            sg.popup(f"{counter_page_already_exists} pagini deja exista! Paginile nu au mai fost create", title="Outcome")
        if counter_page_already_exists == 1 and counter_page_create == 0:
            sg.popup(f"O pagina deja creata exista!", title="Outcome")
        if counter_page_already_exists >= 1 and counter_page_create >= 1:
            sg.popup(f"Au fost generate {counter_page_create} pagini cu succes!",
                     f"{counter_page_already_exists} pagini deja exista! Paginile nu au mai fost create", title="Outcome")
        window.close()



def main():
    """Aici, in loc de variabila, va fi efectuata conexiunea cu baza de date,
    de unde vor fi aduse toate elementele necesare cu ajutorul carora se va crea o instanta de clasa Manual
    """
    manual = Manual(
        isbn5_pag="07016", clasa="1", lang="RO", pag_start=0,
        title="Engleză pentru Clasa I A670.pdf", link_pdf='https://manuale.edu.ro/manuale/Clasa%20I/Comunicare%20in%20limba%20moderna%201%20engleza/EDP/A670.pdf',
        link_md='https://manuale.edu.ro/manuale/Clasa%20I/Comunicare%20in%20limba%20moderna%201%20engleza/EDP/                                                                                                                                                                 ',
        nrPag=117, filename="engleza670"
    ) # variabila de test pentru a testa generatorul de pagini
    app = MainApp()
    while True:
        event, values = app.window.read()
        if (event == sg.WIN_CLOSE_ATTEMPTED_EVENT or event == "Anuleaza") and sg.popup_yes_no("Doriti sa inchideti aplicatia?") == "Yes":
            break
        if event == "Genereaza":
            # sg.popup("Hello dude", "You are on the right track :D", f"Ai introdus urmatoarea valoare: {values['-FILENAME-']}")
            if values["-FILENAME-"] != manual.filename:
                sg.popup("Ati introdus un nume de manual invalid!")
            else:
                app.window_load(fileName=values['-FILENAME-'], manual=manual)
    app.window.close()

if __name__ == "__main__":
    main()
